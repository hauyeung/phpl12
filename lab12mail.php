<html>
<body>
<?php
//get html form values
$name = $_POST["name"];
$email = $_POST["email"];
$emptyname = true;
$emptyemail = true;

//check if all entries are empty
for ($x=0; $x < count($name); $x++)
{
if ($name[$x]!='')
{
$emptyname = false;
break;
}
}

for ($x=0; $x < count($email); $x++)
{
if ($email[$x]!='')
{
$emptyemail = false;
break;
}
}

if ($emptyname || $emptyemail)
{
$url = "http://".$_SERVER['HTTP_HOST']."/lab12.php?noinput";
   header("Location: ".$url);
   exit();
}

//open template
$template = fopen("template.txt","r");
//parse template
$text = fgets($template);

for ($x=0; $x<count($name); $x++)
{
//mail to email address with template message and name
if (preg_match("([a-zA-Z0-9]+@[a-zA-Z0-9_-]+\.[a-zA-Z0-9_-]+)",$email[$x]))
{
mail($email[$x], "Message","Hi ".$name[$x].",\r\n ".$text);
echo "Messages sent to ".$email[$x]."<br>";
}
else
{
echo "Message not sent<br>";
}
}

//close file
fclose($template);

?>
</body>
</html>